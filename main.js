/** Rotation **/
window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;

    // Preload assets.
    game.preload('spaceship.png');
    
    // Specify what should happen when the game loads.
    game.onload = function () {
        game.rootScene.backgroundColor = "black";

        // Make a few sprites, place in a column, rotate them.
        var spaceship1 = new Sprite(48, 32);
        spaceship1.image = game.assets['spaceship.png'];
        spaceship1.x = 135;
        spaceship1.y = 60;

        var spaceship2 = new Sprite(48, 32);
        spaceship2.image = game.assets['spaceship.png'];
        spaceship2.x = 135;
        spaceship2.y = 120;
        spaceship2.rotate(45);     // positive degrees rotates CW

        var spaceship3 = new Sprite(48, 32);
        spaceship3.image = game.assets['spaceship.png'];
        spaceship3.x = 135;
        spaceship3.y = 180;
        spaceship3.rotate(90);
        
        var spaceship4 = new Sprite(48, 32);
        spaceship4.image = game.assets['spaceship.png'];
        spaceship4.x = 135;
        spaceship4.y = 240;
        spaceship4.rotation = 135;  // you can also directly set the property
        
        game.rootScene.addChild(spaceship1);
        game.rootScene.addChild(spaceship2);
        game.rootScene.addChild(spaceship3);            
        game.rootScene.addChild(spaceship4);
    };
    
    // Start the game.
    game.start();
}
